<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfiles extends Model
{

    protected $fillable = ['name', 'username', 'pin','edad'];
    
}
