<?php

namespace App\Http\Controllers;

use App\Perfiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PerfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Perfiles::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Perfiles::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $perfiles = Perfiles::Find($id);
            $perfiles->name= $request->name;
            $perfiles->username =$request->username;
            $perfiles->pin = $request->pin;
            $profiles->edad = $request->edad;
           

            $user->save();

          return ['retorno'=>'ok','data'=>$request->all];
         

      } catch (\Exception $error) {
          return ['retorno'=>'error','details'->$error];

      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Perfiles::where('id', $id)->exists()) {
            $perfiles = Perfiles::find($id);
            $perfiles->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Profile not found"
            ], 404);
          }
        

    }
}
