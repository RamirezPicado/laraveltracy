<?php

namespace App\Http\Controllers;

use App\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
   
    public function index()
    {
        return Register::all();
         
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */
    public function store(Request $request)
    {
        //validation 
        Register::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
    
        //$register->update($request->all());
        //return response()->json($register,200);
              try {
                    $register = Register::Find($id);
                    $register->name= $request->name;
                    $register->last_name =$request->last_name;
                    $register->email = $request->email;
                    $register->country = $request->country;
                    $register->birth_date = $request->birth_date;
                    $register->phone = $request->phone;

                    $register->save();

                  return ['retorno'=>'ok','data'=>$request->all];
                 

              } catch (\Exception $error) {
                  return ['retorno'=>'error','details'->$error];

              }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
        if(Register::where('id', $id)->exists()) {
            $register = Register::find($id);
            $register->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Register not found"
            ], 404);
          }
        

    }


    



}
