<?php

namespace App\Http\Controllers;
use App\Playlist;
use Illuminate\Http\Request;



class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     */
    public function create()
    {
        // 

        $playlist = new Playlist;
        $playlist->name=$request->name;
        $playlist->save();



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */
    public function store(Request $request)
    {

       // 
        return Playlist::create($request->all());


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //::where('id', $id)->get();
       // return Playlist::where('id', $id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Playlist::where('id', $id)->exists()) {
            $playlist = Playlist::find($id);
            $playlist ->name = is_null($request->name) ? $playlist->name : $request->name;
            $playlist->save();
    
            return response()->json([
                "message" => "records updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Student not found"
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Playlist::where('id', $id)->exists()) {
            $video = Playlist::find($id);
            $video->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Playlist not found"
            ], 404);
          }
        

    }
}
