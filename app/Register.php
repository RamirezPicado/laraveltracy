<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Register extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = ['name', 'last_name', 'email','password', 'country', 'birth_date',  'phone'];
        
        
    }

    