<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 Route::middleware('auth:api')->get('/login', function (Request $request) {
     return $request->login();
});

 Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
   });




 Route::post('/user', 'UserController@register');
//Route::post('/playlists', 'PlaylistController@store');

Route::resource('/playlists','PlaylistController');

Route::resource('/perfiles','PerfilesController');

Route::resource('/registers', 'RegisterController');

 Route::group(['middleware' => ['jwt.verify']], function() {

    
   
  
   
//    // Route::get('closed', 'DataController@closed');
//    // Route::get('/videos', 'VideoController@index');
//     //movies
//     //profiles
//     //playlist
    
// });
});